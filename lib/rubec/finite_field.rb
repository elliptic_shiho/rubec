require 'rubec/arithmetic'

module Rubec
  class FiniteField
    def initialize(p)
      @p = p
    end

    def p
      @p
    end

    def add(left, right)
      (left + right) % @p
    end

    def sub(left, right)
      (left - right) % @p
    end

    def mul(left, right)
      (left * right) % @p
    end

    def inv(op)
      Rubec::modinv(op, @p)
    end

    def div(left, right)
      (left * Rubec::modinv(right, @p)) % @p
    end

    def pow(op, exp)
      op.pow(exp, @p)
    end

    def equ(left, right)
      (left % @p) == (right % @p)
    end

    def ==(other)
      other.is_a?(FiniteField) and other.p == @p
    end
  end
end
