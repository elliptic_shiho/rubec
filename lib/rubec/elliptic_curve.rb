module Rubec 
  class ECPoint
    def initialize (curve, x, y, z)
      @curve = curve
      @f = curve.field
      @x = x
      @y = y
      @z = z
    end

    def curve
      @curve
    end

    def x
      @x
    end

    def y
      @y
    end

    def z
      @z
    end

    def infinity?
      @f.equ(@x, 0) and @f.equ(@z, 0) and @f.equ(@y, 1)
    end

    def normalize
      ECPoint.new(@curve, @f.div(@x, @z), @f.div(@y, @z), 1)
    end

    def ==(other)
      ret = false
      if other.is_a?(ECPoint) and other.curve == @curve
        ret = @f.equ(@f.mul(@x, other.y), @f.mul(@y, other.x))
      end
      ret
    end
  end

  class EllipticCurve
    def initialize (field, a, b)
      @f = field
      @A = a
      @B = b
      @O = ECPoint.new(self, 0, 1, 0)
    end

    def field
      @f
    end

    def A
      @A
    end

    def B
      @B
    end

    def add(_P, _Q)
      raise unless _P.is_a?(ECPoint) and _Q.is_a?(ECPoint) and "Invalid Arguments"
      if _P.infinity?
        res = _Q.clone
      elsif _Q.infinity?
        res = _P.clone
      elsif @f.equ(_P.x, _Q.x) and @f.equ(_P.y + _Q.y, 0)
        res = @O.clone
      else
        rx = 0
        ry = 0
        rz = 0
        if _P == _Q
          x = _P.x
          y = _P.y
          z = _P.z
          a = @f.mul(@f.mul(3, x), x)     # 3x^2
          b = @f.mul(@f.mul(@A, z), z)    # Az^2
          u = @f.add(a, b)                # 3x^2 + Az^2
          
          v = @f.mul(y, z)                # yz

          yv = @f.mul(y, v)               # yv
          yv4 = @f.mul(v, 4)              # 4yv
          
          a = @f.mul(u, u)                # u^2
          b = @f.mul(@f.mul(2, x), yv4)   # 2x * 4yv
          w = @f.sub(a, b)                # u^2 - 2x * yv4

          rx = @f.mul(@f.mul(2, v), w)    # Rx = 2vw

          a = @f.sub(@f.mul(x, yv4), w)   # x*yv4 - w
          a = @f.mul(a, u)                # u(x*yv4 - w)
          b = @f.mul(@f.mul(2, yv4), yv)  # 8(yv)^2
          ry = @f.sub(a, b)               # Ry = u(x*yv4 - w) - 8(yv)^2
          
          # Rz = 8v^3
          rz = @f.mul(@f.mul(8, v), @f.mul(v, v))
        else
          a = @f.mul(_Q.y, _P.z)          # QyPz
          b = @f.mul(_P.y, _Q.z)          # PyQz
          u = @f.sub(a, b)                # QyPz - PyQz

          a = @f.mul(_Q.x, _P.z)          # QxPz
          pxqz = @f.mul(_P.x, _Q.z)       # PxQz
          v = @f.sub(a, pxqz)             # QxPz - PxQz

          v2 = @f.mul(v, v)               # v^2
          v3 = @f.mul(v2, v)              # v^3

          a = @f.mul(u, u)                # u^2
          b = @f.mul(_P.z, _Q.z)          # PzQz
          a = @f.mul(a, b)                # u^2 * PzQz
          a = @f.sub(a, v3)               # u^2 * PzQz - v^3
          b = @f.mul(2, v2)               # 2v^2
          b = @f.mul(b, pxqz)             # 2 PxQz v^2
          w = @f.sub(a, b)                # u^2 PzQz - v^3 - 2 PxQz v^2

          rx = @f.mul(v, w)               # Rx = vw

          a = @f.mul(v2, _P.x)            # Px v^2
          a = @f.mul(a, _Q.z)             # Px Qz v^2
          a = @f.sub(a, w)                # Px Qz v^2 - w
          a = @f.mul(u, a)                # u(Px Qz v^2 - w)
          b = @f.mul(v3, _P.y)            # Py v^3
          b = @f.mul(b, _Q.z)             # Py Qz v^3
          
          ry = @f.sub(a, b)               # Ry = u(Px Qz v^2 - w) - Py Qz v^3

          # Rz = Pz Qz v^3
          rz = @f.mul(@f.mul(v3, _P.z), _Q.z) 
        end
        res = ECPoint.new(self, rx, ry, rz)
      end

      res
    end

    def mul(n, _P)
      raise unless _P.is_a?(ECPoint) and n.is_a?(Integer) and "Invalid Arguments"
      raise unless 0 < n and "negative multiplication is not supported yet"
      x = _P
      res = @O
      n.bit_length.times { |i|
        res = self.add(res, x) if n[i] == 1
        x = self.add(x, x)
      }
      res
    end

    def ==(other)
      ret = false
      if other.is_a?(EllipticCurve) and other.field == @f
        ret = @f.equ(@A, other.A) and @f.equ(@B, other.B)
      end
      ret
    end
  end

end
