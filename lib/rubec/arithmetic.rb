module Rubec
  def self.gcd(a, b)
    while b != 0
      a, b = b, a % b
    end
    a
  end

  def self.egcd(a, b)
    x, y, u, v = 0, 1, 1, 0
    while a != 0
      q, r = b / a, b % a
      m, n = x - u * q, y - v * q
      a, b, x, y, u, v = r, a, u, v, m, n
    end
    [b, x, y]
  end

  def self.modinv(a, m)
    g, x, y = egcd(a, m)
    raise unless g == 1 and "Can't compute modular inverse of #{a} over Z/#{m}Z"
    return x % m
  end
end

