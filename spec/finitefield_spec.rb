require 'spec_helper'

describe Rubec do
  it 'has basic arithmetic operations over Z/pZ (where p is a prime))' do
    f = Rubec::FiniteField.new(5)
    x = 3
    y = 7
    expect(f.add(x, y)).to eq 0
    expect(f.sub(x, y)).to eq 1
    expect(f.mul(x, y)).to eq 1
    x = 2
    y = 3
    expect(f.inv(x)).to eq y
    expect(f.inv(y)).to eq x
    expect(f.inv(y)).to eq x
    expect(f.pow(x, 3)).to eq y
    expect(f.equ(3, 8)).to eq true
  end
end
